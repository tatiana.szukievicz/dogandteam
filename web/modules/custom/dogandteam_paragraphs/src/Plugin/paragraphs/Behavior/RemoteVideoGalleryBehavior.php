<?php

namespace Drupal\dogandteam_paragraphs\Plugin\paragraphs\Behavior;


use Drupal\Component\Utility\Html;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Annotation\ParagraphsBehavior;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * @ParagraphsBehavior(
 *   id = "dogandteam_paragraphs_remote_video_gallery",
 *   label = @Translation("Remote video gallery settings"),
 *   description= @Translation("Settings for remote video gallery paragraph type."),
 *   weight = 0,
 * )
 */
class RemoteVideoGalleryBehavior extends ParagraphsBehaviorBase {

  /**
   * @inheritDoc
   */
  public static function isApplicable(ParagraphsType $paragraphs_type) {
    return $paragraphs_type->id === 'remote_video_gallery';
  }

  /**
   * Extends the paragraph render array with behavior.
   *
   * @param array &$build
   *   A renderable array representing the paragraph. The module may add
   *   elements to $build prior to rendering. The structure of $build is a
   *   renderable array as expected by drupal_render().
   * @param \Drupal\paragraphs\Entity\Paragraph $paragraph
   *   The paragraph.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The entity view display holding the display options configured for the
   *   entity components.
   * @param string $view_mode
   *   The view mode the entity is rendered in.
   *
   * @return array
   *   A render array provided by the plugin.
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode)  {
    $videos_per_row = $paragraph->getBehaviorSetting($this->getPluginId(),'items_per_row', 4);
    $bem_block = 'paragraphs-' . $paragraph->bundle() . ($view_mode === 'default' ? '' : '-' . $view_mode);

    $build['#attributes']['class'][] = Html::getClass($bem_block . '--videos-per-row-' . $videos_per_row);
  }

  /**
   * @inheritDoc
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $form['items_per_row'] = [
      '#type' => 'select',
      '#title' => $this->t('Number videos per row.'),
      '#description' => 'Selection the number of videos per row',
      '#options' => $this->getItemsPerRowOptions(),
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(),'items_per_row', 4),
    ];

    return $form;
  }

  /**
   * @inheritDoc
   */
  public function settingsSummary(Paragraph $paragraph) {
    $items_per_row = $paragraph->getBehaviorSetting($this->getPluginId(),'items_per_row', 4);
    $items_per_row_options = $this->getItemsPerRowOptions();

    $summary = [];
    $summary[] = $this->t('Videos per row: @value', ['@value' => $items_per_row_options[$items_per_row]]);

    return $summary;
  }

  private function getItemsPerRowOptions() {

    return [
      '2' => $this->formatPlural(2, '1 video per row', '@count videos per row'),
      '3' => $this->formatPlural(3, '1 video per row', '@count videos per row'),
      '4' => $this->formatPlural(4, '1 video per row', '@count videos per row'),
    ];
  }
}
