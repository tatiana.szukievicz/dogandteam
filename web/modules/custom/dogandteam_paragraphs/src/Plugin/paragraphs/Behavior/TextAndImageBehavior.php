<?php

namespace Drupal\dogandteam_paragraphs\Plugin\paragraphs\Behavior;


use Drupal\Component\Utility\Html;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Annotation\ParagraphsBehavior;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * @ParagraphsBehavior(
 *   id = "dogandteam_paragraphs_text_and_image",
 *   label = @Translation("Text and image paragraph settings"),
 *   description= @Translation("Settings for Text and image paragraph type. Provides position and size for image"),
 *   weight = 0,
 * )
 */
class TextAndImageBehavior extends ParagraphsBehaviorBase {

  /**
   * @inheritDoc
   */
  public static function isApplicable(ParagraphsType $paragraphs_type) {
    return $paragraphs_type->id === 'text_and_image';
  }

  /**
   * Extends the paragraph render array with behavior.
   *
   * @param array &$build
   *   A renderable array representing the paragraph. The module may add
   *   elements to $build prior to rendering. The structure of $build is a
   *   renderable array as expected by drupal_render().
   * @param \Drupal\paragraphs\Entity\Paragraph $paragraph
   *   The paragraph.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The entity view display holding the display options configured for the
   *   entity components.
   * @param string $view_mode
   *   The view mode the entity is rendered in.
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode)  {
    $bem_block = 'paragraphs-' . $paragraph->bundle() . ($view_mode === 'default' ? '' : '-' . $view_mode);
    $image_size = $paragraph->getBehaviorSetting($this->getPluginId(),'image_size', '4_12');
    $image_position = $paragraph->getBehaviorSetting($this->getPluginId(),'image_position', 'top');

    $build['#attributes']['class'][] = Html::getClass($bem_block . '--image-size-' . $image_size);
    $build['#attributes']['class'][] = Html::getClass($bem_block . '--image-position-' . $image_position);

    if (isset($build['field_image']) && $build['field_image']['#formatter'] === 'media_thumbnail') {
      switch ($image_size) {
        case '4_12':
        default:
          $image_style = 'paragraph_image_text_4_of_12';
          break;

        case '6_12':
          $image_style = 'paragraph_image_text_6_of_12';
          break;

        case '8_12':
          $image_style = 'paragraph_image_text_8_of_12';
          break;

        case '12_12':
          $image_style = 'paragraph_image_text_12_of_12';
          break;
      }

      $build['field_image'][0]['#image_style'] = $image_style;
    }
  }

  /**
   * @inheritDoc
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $form['image_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Image size'),
      '#description' => 'Selection the size of image in grid 12',
      '#options' => $this->getImageSizeOptions(),
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(),'image_size', '4_12'),
    ];
    $form['image_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Image position'),
      '#description' => 'Selection the position of image',
      '#options' => $this->getImagePositionOptions(),
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(),'image_position', 'top'),
    ];

    return $form;
  }

  /**
   * @inheritDoc
   */
  public function settingsSummary(Paragraph $paragraph) {
    $image_size = $paragraph->getBehaviorSetting($this->getPluginId(),'image_size', '4_12');
    $image_position = $paragraph->getBehaviorSetting($this->getPluginId(),'image_position', 'top');

    $image_size_options = $this->getImageSizeOptions();
    $image_position_options = $this->getImagePositionOptions();

    $summary = [];
    $summary[] = $this->t('Image size: @value', ['@value' => $image_size_options[$image_size]]);
    $summary[] = $this->t('Image position: @value', ['@value' => $image_position_options[$image_position]]);

    return $summary;
  }

  private function getImageSizeOptions() {

    return [
      '4_12' => $this->t('4 columns of 12'),
      '6_12' => $this->t('6 columns of 12'),
      '8_12' => $this->t('8 columns of 12'),
      '12_12' => $this->t('12 columns of 12'),
    ];
  }

  private function getImagePositionOptions() {

    return [
      'top' => $this->t('Top'),
      'left' => $this->t('Left'),
      'right' => $this->t('Right'),
    ];
  }
}
