<?php

namespace Drupal\dogandteam_paragraphs\Plugin\paragraphs\Behavior;


use Drupal\Component\Utility\Html;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Annotation\ParagraphsBehavior;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * @ParagraphsBehavior(
 *   id = "dogandteam_paragraphs_paragrap_class",
 *   label = @Translation("Custom classes for paragraph"),
 *   description= @Translation("Allows to add custom classe to paragraph"),
 *   weight = 0,
 * )
 */
class ParagraphClassBehavior extends ParagraphsBehaviorBase {

  /**
   * @inheritDoc
   */
  public static function isApplicable(ParagraphsType $paragraphs_type) {
    return TRUE;
  }

  /**
   * Extends the paragraph render array with behavior.
   *
   * @param array &$build
   *   A renderable array representing the paragraph. The module may add
   *   elements to $build prior to rendering. The structure of $build is a
   *   renderable array as expected by drupal_render().
   * @param \Drupal\paragraphs\Entity\Paragraph $paragraph
   *   The paragraph.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The entity view display holding the display options configured for the
   *   entity components.
   * @param string $view_mode
   *   The view mode the entity is rendered in.
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {
    $classes = $paragraph->getBehaviorSetting($this->getPluginId(),'classes', '');
    $classes_array = explode(" ", $classes);

    foreach ($classes_array as $class) {
      $build['#attributes']['class'][] = Html::getClass($class);
    }
  }

  /**
   * @inheritDoc
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $form['classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Classes'),
      '#description' => 'Custom classes for paragraph separated by space. They are processed via Html::getClass().',
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(),'classes', ''),
    ];

    return $form;
  }

  /**
   * @inheritDoc
   */
  public function settingsSummary(Paragraph $paragraph) {
    $classes = $paragraph->getBehaviorSetting($this->getPluginId(),'classes', '');

    $summary = [];
    $summary[] = $this->t('Classes: @value', ['@value' => $classes]);

    return $summary;
  }

}
