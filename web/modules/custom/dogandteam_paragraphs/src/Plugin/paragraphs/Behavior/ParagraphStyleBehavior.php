<?php

namespace Drupal\dogandteam_paragraphs\Plugin\paragraphs\Behavior;


use Drupal\Component\Utility\Html;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Annotation\ParagraphsBehavior;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * @ParagraphsBehavior(
 *   id = "dogandteam_paragraphs_paragrap_style",
 *   label = @Translation("Paragraphs style"),
 *   description= @Translation("Allows to select predifined style for paragrafhs"),
 *   weight = 0,
 * )
 */
class ParagraphStyleBehavior extends ParagraphsBehaviorBase {

  /**
   * @inheritDoc
   */
  public static function isApplicable(ParagraphsType $paragraphs_type) {
    return TRUE;
  }

  /**
   * Extends the paragraph render array with behavior.
   *
   * @param array &$build
   *   A renderable array representing the paragraph. The module may add
   *   elements to $build prior to rendering. The structure of $build is a
   *   renderable array as expected by drupal_render().
   * @param \Drupal\paragraphs\Entity\Paragraph $paragraph
   *   The paragraph.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The entity view display holding the display options configured for the
   *   entity components.
   * @param string $view_mode
   *   The view mode the entity is rendered in.
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {
    $classes = $paragraph->getBehaviorSetting($this->getPluginId(),'styles', []);
    $bem_block = 'paragraphs-' . $paragraph->bundle() . ($view_mode === 'default' ? '' : '-' . $view_mode);
    foreach ($classes as $class) {
      $build['#attributes']['class'][] = $bem_block . '--' . Html::getClass($class);
    }
  }

  /**
   * @inheritDoc
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $form['style_wrapper'] = [
      '#type' => 'details',
      '#title' => $this->t('Paragraph style'),
      '#open' => FALSE,
    ];

    $styles = $this->getStyles($paragraph);
    $selected_styles = $paragraph->getBehaviorSetting($this->getPluginId(),'styles', []);

    foreach ($styles as $group_id => $group) {
      $form['style_wrapper'][$group_id] = [
        '#type' => 'checkboxes',
        '#title' => $group['label'],
        '#description' => 'Predefined styles for paragraphs',
        '#options' => $group['options'],
        '#default_value' => $selected_styles,
      ];

    }

    return $form;
  }

  /**
   * @inheritDoc
   */
  public function submitBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $styles = [];
    $filtered_values = $this->filterBehaviorFormSubmitValues($paragraph, $form, $form_state);
    if (!empty($filtered_values)) {
      $style_groups = $filtered_values['style_wrapper'];

      foreach ($style_groups as $group) {
        foreach ($group as $style_name) {
          $styles[] = $style_name;
        }
      }
      $paragraph->setBehaviorSettings($this->getPluginId(), ['styles' => $styles]);
    }

  }

  /**
   * @inheritDoc
   */
  public function settingsSummary(Paragraph $paragraph) {
    $classes = $paragraph->getBehaviorSetting($this->getPluginId(),'styles', []);

    $summary = [];
    foreach ($classes as $class) {
      $summary[] = $this->t('Styles: @value', ['@value' => $class]);
    }

    return $summary;
  }

  /**
   * Returns styles for paragraph.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *
   * @return array
   */
  public function getStyles(ParagraphInterface $paragraph) {
    $styles = [];

    if ($paragraph->hasField('field_title')) {
      $styles['title'] = [
        'label' => $this->t('Paragraphs title'),
        'options' => [
          'title_bold' => $this->t('Bold'),
          'title_centered' => $this->t('Centered'),
        ],
      ];

    }

    if ($paragraph->hasField('field_image')) {
      $styles['image'] = [
        'label' => $this->t('Paragraphs image'),
        'options' => [
          'image_circled' => $this->t('Image circled'),
        ],
      ];
    }

    $styles['common'] = [
      'label' => $this->t('Paragraphs common styles'),
      'options' => [
        'style_black' => $this->t('Style black'),
      ],
    ];

    return $styles;
  }
}
