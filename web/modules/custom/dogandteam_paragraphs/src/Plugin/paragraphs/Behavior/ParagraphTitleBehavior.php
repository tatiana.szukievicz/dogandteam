<?php

namespace Drupal\dogandteam_paragraphs\Plugin\paragraphs\Behavior;


use Drupal\Component\Utility\Html;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Annotation\ParagraphsBehavior;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * @ParagraphsBehavior(
 *   id = "dogandteam_paragraphs_paragraph_title",
 *   label = @Translation("Paragraph title settings"),
 *   description= @Translation("Allows to select the settings for title "),
 *   weight = 0,
 * )
 */
class ParagraphTitleBehavior extends ParagraphsBehaviorBase {

  /**
   * @inheritDoc
   */
  public static function isApplicable(ParagraphsType $paragraphs_type) {
    return TRUE;
  }

  /**
   * Extends the paragraph render array with behavior.
   *
   * @param array &$build
   *   A renderable array representing the paragraph. The module may add
   *   elements to $build prior to rendering. The structure of $build is a
   *   renderable array as expected by drupal_render().
   * @param \Drupal\paragraphs\Entity\Paragraph $paragraph
   *   The paragraph.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The entity view display holding the display options configured for the
   *   entity components.
   * @param string $view_mode
   *   The view mode the entity is rendered in.
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode)  {

  }

  /**
   * @inheritDoc
   */
  public function preprocess(&$variables) {
    /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
    $paragraph = $variables['paragraph'];
    $variables['title_wrapper'] = $paragraph->getBehaviorSetting($this->getPluginId(),'title_element', 'h2');
  }

  /**
   * @inheritDoc
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    if ($paragraph->hasField('field_title')) {
      $form['title_element'] = [
        '#type' => 'select',
        '#title' => $this->t('Title wrapper'),
        '#description' => 'Wrapper HTML element',
        '#options' => $this->getTitleOptions(),
        '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(),'title_element', 'h2'),
      ];
    }

    return $form;
  }

  /**
   * @inheritDoc
   */
  public function settingsSummary(Paragraph $paragraph) {

    $title_element = $paragraph->getBehaviorSetting($this->getPluginId(),'title_element', 'h2');
    $title_element_options = $this->getTitleOptions();

    $summary = [];
    $summary[] = $this->t('Title element: @value', ['@value' => $title_element_options[$title_element]]);

    return $summary;
  }

  private function getTitleOptions() {
    return [
      'h2' => $this->t('h2'),
      'h3' => $this->t('h3'),
      'h4' => $this->t('h4'),
      'div' => $this->t('div'),
    ];
  }

}
