<?php

namespace Drupal\drupal_matrix_security\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for drupal_matrix_security routes.
 */
class DrupalMatrixSecurityCSRFAccessControler extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    return [
      '#theme' => 'drupal_matrix_security',
    ];
  }

}
